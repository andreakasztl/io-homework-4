import java.time.Duration;
import java.util.Objects;

public class Athlete {
    private int number;
    private  String name;
    private String ctyCode;
    private Duration time;
    private Duration finalTime;
    private int penaltySeconds;
    private int[] firstShootingRange;
    private int[] secondShootingRange;
    private int[] thirdShootingRange;

    public Athlete(int number, String name, String ctyCode, Duration time) {
        this.number = number;
        this.name = name;
        this.ctyCode = ctyCode;
        this.time = time;
        penaltySeconds = 0;
        firstShootingRange = new int[5];
        secondShootingRange = new int[5];
        thirdShootingRange = new int[5];
    }

    public int[] getFirstShootingRange() {
        return firstShootingRange;
    }

    public int[] getSecondShootingRange() {
        return secondShootingRange;
    }

    public int[] getThirdShootingRange() {
        return thirdShootingRange;
    }

    public void setFirstShootingRange(int[] firstShootingRange) {
        this.firstShootingRange = firstShootingRange;
    }

    public void setSecondShootingRange(int[] secondShootingRange) {
        this.secondShootingRange = secondShootingRange;
    }

    public void setThirdShootingRange(int[] thirdShootingRange) {
        this.thirdShootingRange = thirdShootingRange;
    }

    public void setShootingRange(String firstShootingRange, String secondShootingRange, String thirdShootingRange){

        this.firstShootingRange = getShootingRangeArray(firstShootingRange);
        this.secondShootingRange = getShootingRangeArray(secondShootingRange);
        this.thirdShootingRange = getShootingRangeArray(thirdShootingRange);

    }

    private int[] getShootingRangeArray(String result){
        if (result.length() != 5)
            throw new IllegalArgumentException("The shooting range result must contain five characters");

        int [] srArray = new int[5];
        int i = 0;

        for(char r : result.toCharArray()){
            switch(r){
                //miss
                case 'o': srArray[i] = 1; break;
                //hit
                case 'x': srArray[i] = 0; break;
                default: throw new IllegalArgumentException("Incorrect shooting range results.");
            }
            i++;
        }
        return srArray;
    }

    public void setFinalTime(Duration finalTime){
        this.finalTime = finalTime;
    }
    public void setFinalTime(){
        Duration fT = time;
        for ( int i : firstShootingRange){
            if ( i == 1 ){
                fT = fT.plusSeconds(10);
                penaltySeconds += 10;
            }

        }

        for ( int i : secondShootingRange){
            if ( i == 1 ){
                fT = fT.plusSeconds(10);
                penaltySeconds += 10;
            }
        }

        for ( int i : thirdShootingRange){
            if ( i == 1 ){
                fT = fT.plusSeconds(10);
                penaltySeconds += 10;
            }
        }

        finalTime = fT;
    }

    public Duration getTime() {
        return time;
    }

    public String getName() {
        return name;
    }

    public Duration getFinalTime() {
        return finalTime;
    }

    public int getPenaltySeconds() {
        return penaltySeconds;
    }
}
