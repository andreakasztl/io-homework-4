import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;

public class SkiBiathlon {
    private List<Athlete> contestants;

    public SkiBiathlon() {
        contestants = new ArrayList<>();
    }

    public void setContestants(File csvFile){


        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            setContestants(br);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setContestants(BufferedReader br) throws IOException {
        String line;
        String cvsDelimiter = ",";

        while ((line = br.readLine()) != null) {

            String[] results = line.split(cvsDelimiter);

            int number = Integer.parseInt(results[0]);
            String name = results[1];
            String country = results[2];

            Duration time = Duration.parse("PT" + results[3].replace(':', 'M').concat("S"));

            Athlete athlete = new Athlete(number, name,country, time);
            athlete.setShootingRange(results[4], results[5], results[6]);
            athlete.setFinalTime();

            contestants.add(athlete);
        }
    }

    public void addContestant(Athlete athlete){
        contestants.add(athlete);
    }

    public List<Athlete> getContestants() {
        ArrayList<Athlete> contectantsCopy = new ArrayList();
        contectantsCopy.addAll(contestants);
        return contectantsCopy;
    }

    public Map<Integer, Athlete> getWinners(){
        HashMap<Integer, Athlete> winners = new HashMap<>();
        Collections.sort(contestants,new FinalTimeComparator());

        winners.put(1,contestants.get(0));
        winners.put(2,contestants.get(1));
        winners.put(3,contestants.get(2));
        return winners;
    }




}
