import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        SkiBiathlon skiBiathlon = new SkiBiathlon();
        skiBiathlon.setContestants(new File("biathlon.csv"));
        Map<Integer, Athlete> win = skiBiathlon.getWinners();
        //Winner - Piotr Smitzer 30:10 (30:10 + 0)
        System.out.println("Winner - " + win.get(1).getName() + " " + durationString(win.get(1).getFinalTime()) + " (" + durationString(win.get(1).getTime()) + " + " + win.get(1).getPenaltySeconds() + ")");
        System.out.println("Runner-up - " + win.get(2).getName() + " " + durationString(win.get(2).getFinalTime()) + " (" + durationString(win.get(2).getTime()) + " + " + win.get(2).getPenaltySeconds() + ")");
        System.out.println("Third Place - " + win.get(3).getName() + " " + durationString(win.get(3).getFinalTime()) + " (" + durationString(win.get(3).getTime()) + " + " + win.get(3).getPenaltySeconds() + ")");

        saveWinners(win);
    }

    public static String durationString(Duration duration){
        return duration.toString()
                .substring(2)
                .replace("H",":")
                .replace("M",":")
                .replace("S","");

    }

    public static void saveWinners(Map<Integer, Athlete> win){

        try {

            Path path = Paths.get("results");

            Files.createDirectories(path);
            File myFile = new File("results/winners.txt");
            if (myFile.createNewFile()) {
                System.out.println("File created: " + myFile.getName());
            }

            FileWriter fileWriter = new FileWriter(myFile);
            fileWriter.write("Winner - " + win.get(1).getName() + " " + durationString(win.get(1).getFinalTime()) + " (" + durationString(win.get(1).getTime()) + " + " + win.get(1).getPenaltySeconds() + ")\n");
            fileWriter.append("Runner-up - " + win.get(2).getName() + " " + durationString(win.get(2).getFinalTime()) + " (" + durationString(win.get(2).getTime()) + " + " + win.get(2).getPenaltySeconds() + ")\n");
            fileWriter.append("Third Place - " + win.get(3).getName() + " " + durationString(win.get(3).getFinalTime()) + " (" + durationString(win.get(3).getTime()) + " + " + win.get(3).getPenaltySeconds() + ")\n");
            fileWriter.close();


        } catch (IOException e) {

            System.out.println("biiii");
            System.out.println(e);

        }
    }



}
