import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.time.Duration;

import java.util.Map;
import java.util.Random;

import static org.mockito.Mockito.when;


public class SkiBiathlonTest {

    @Test
    public void getWinnersTest(){
        SkiBiathlon skiBiathlon = new SkiBiathlon();
        Random r = new Random();

        Athlete athlete = Mockito.mock(Athlete.class);
        when(athlete.getFinalTime()).thenReturn(Duration.parse("PT30M25S"));
        when(athlete.getName()).thenReturn("First");
        skiBiathlon.addContestant(athlete);

        Athlete athlete2 = Mockito.mock(Athlete.class);
        when(athlete2.getFinalTime()).thenReturn(Duration.parse("PT31M25S"));
        when(athlete2.getName()).thenReturn("Second");
        skiBiathlon.addContestant(athlete2);

        Athlete athlete3 = Mockito.mock(Athlete.class);
        when(athlete3.getFinalTime()).thenReturn(Duration.parse("PT33M25S"));
        when(athlete3.getName()).thenReturn("Third");
        skiBiathlon.addContestant(athlete3);



        Map<Integer,Athlete> winners = skiBiathlon.getWinners();
        Assert.assertEquals("First",winners.get(1).getName());
        Assert.assertEquals("Second",winners.get(2).getName());
        Assert.assertEquals("Third",winners.get(3).getName());


    }


    @Test
    public void setContestantsTest() throws IOException {
        String data =
                "11,Umar Jorgson,SK,30:27,xxxox,xxxxx,xxoxo\n" +
                 "1,Jimmy Smiles,UK,29:15,xxoox,xooxo,xxxxo\n" +
                 "27,Piotr Smitzer,CZ,30:10,xxxxx,xxxxx,xxxxx\n" +
                 "32,Johannes Thingnes,NO,28:00,xxxxx,xxoxx,xxxxx";
        SkiBiathlon skiBiathlon = new SkiBiathlon();
        BufferedReader bufferedReader = new BufferedReader(new StringReader(data));
        skiBiathlon.setContestants(bufferedReader);

        Assert.assertEquals(4, skiBiathlon.getContestants().size());
    }
}
